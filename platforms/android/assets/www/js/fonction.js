/*****************************************************************************/
//	Auteur:				Samuel
//	Description:		fonction.js
//	Date de création:	Hivers 2015
//	But:				-Contenir les question , réponses et Images 
//	
//	Version		Date 			Nom 			                     Description 
//	1.0			25 fev  2015	Samuel      	                     Création de la page
//  2.0         24 mars 2015    Michaël Bourgoin et Samuel Jacques   Commentaire 
/*****************************************************************************/

//Fonction ActionInRange actuellement utilisé ou non 
var inuse = false;
var nomLocalStorage = "table";

//Permet l'utilisation de changer de bouton lors des choix multiples
//Params : this.id 
function ChangerBouton(id)
{
    document.getElementById(id).style.backgroundColor = "#006f51";
    document.getElementById(id).setAttribute("name", "reponse");
    
    var Parent = document.getElementById(id).parentNode;
    var bouton = document.getElementsByClassName("question");
    
    //Boucle permettant d'enlevé l'attribut des autres boutons et les change de couleurs (rouge)
    for (var i = 0; i < bouton.length; i++) {
        if(bouton[i].id != id)
        {
            var BoutonParent = document.getElementById(bouton[i].id).parentNode;   
            if(BoutonParent.id == Parent.id)
            {
                document.getElementById(bouton[i].id).style.backgroundColor = "#7f2729";
                document.getElementById(bouton[i].id).removeAttribute("name");
            }
        }
    }
}

//Permet l'éxécution d'un tab automatique sur les questions a multiples textbox et réduit en lower case  
//Params : this.id 
function ChangeTab(id)
{
    document.getElementById(id).value = document.getElementById(id).value.toLowerCase();
    if(document.getElementById(id).value == "" || document.getElementById(id).value == " ")
    {
        return;
    }
    
    id = parseInt(id);
    if(id != 9)
    {
        id += 1;
    }
    else
        id = 1;
    
    while(document.getElementById(id).disabled == true)
    {
        id +=1;
    }
    
    document.getElementById(id).focus();
}

//Permet de valider les multiples question
//Params : Numéro de la question int
function ValiderMultipleQuestions(PageActuelle)
{
    var numero = PageActuelle[1];
    reponseFinale = "";
    var multipleReponse = document.getElementsByName("reponse");
    var nombreReponse = multipleReponse.length;
    var nombreQuestion = 0;
    var valide = true;
    
    while(document.getElementById("Reponse"+nombreQuestion) !== null)
    {        
        nombreQuestion ++;       
    }
    
    //Boucle permettant de réduire le nombreReponse si les inputs de type texte ont un parent qui s'appel les inputs
    //Ceci empeche un grand nombre de reponse puisque toutes les textbox ont le name reponses
    for(var i=0; i<multipleReponse.length;i++)
    {
        if(multipleReponse[i].type == "text")
        {
            if(multipleReponse[i].parentNode.className == "LesInputs")
            {
                nombreReponse--;
            }
            if(multipleReponse[i].value == "")
            {
                valide = false;
            }
        }
    }
    
    //On ajout au compteur les parents LesInputs  
    nombreReponse += document.getElementsByClassName("LesInputs").length;
    
    if(nombreReponse < nombreQuestion || !valide)
    {
        alert('Vous devez répondre aux questions');
        return;
    }
    //Boucle qui fait la réponse complete  
    for(var i=0;i<nombreQuestion; i++)
    {
        for(j=0;j<multipleReponse.length;j++)
        {
            var rep = "Reponse"+i;
            if(multipleReponse[j].parentNode.id == rep)
            {
                if(multipleReponse[j].type == "text" || multipleReponse[j].type == "number")
                {
				
                    reponseFinale += multipleReponse[j].value.trim();
                    
                }
                else if ( document.getElementById("Image") != null)
                {
                    var Texte = multipleReponse[j].innerHTML.split("/");
                    reponseFinale += Texte[Texte.length-1].replace('">',"");
                }
                else
                {
                    reponseFinale += multipleReponse[j].innerHTML;

                }
            }
        }
        if (i <nombreQuestion-1)
        {
            reponseFinale += "|";
        }
    }
    
    //Appel la fonction dans xml.js
    request(readDataAnswer ,'Answer');
}

//Permet d'initialiser les cookies et de modifier leur valeur.
function SetLocalStorage(name, value) 
{
    window.localStorage.setItem(name, value);
}
// permet d'obtenir la valeur du cookie demander.
function GetLocalStorage(name) 
{
    return window.localStorage.getItem(name);
}
//Permet d'indiquer la réponse de l'utilisateur dans les cookies.
function AddAnswer(question,reussi,nombreQuestion)
{
    var valueLocalStorage;
    var numberOfEstimote;
    var splitEstimote;
    var splitInfo;
    var splitQuestion;
     if(GetLocalStorage(nomLocalStorage) == "")
        Quitter();
    // Va chercher la valeur actuel dans le cookie
    valueLocalStorage = GetLocalStorage(nomLocalStorage);
    //Permet de séparer chaque question et ça réponse et de les mettres dans un tableau.
    numberOfEstimote = EstimoteToQuestion(question);
    if(numberOfEstimote == "")
        ChangePage("attente");
    splitEstimote = valueLocalStorage.split("|");
    //Permet de séparer le numéro de la question de la réponse à changer et de la mettre dans un tableau.
    splitInfo = splitEstimote[numberOfEstimote].split("-");
    splitQuestion = splitInfo[4].split(",");
    for(var i = 0; i < splitQuestion.length ; i++)
    {
        if(splitQuestion[i] == question)
        {
            splitQuestion[i] = question + ":" + reussi + "/" + nombreQuestion;
            break;
        }
    }
    //On reprend la valeur de chaque question et on refait la valeur du cookie.
    splitInfo[4] = "";
    for (var i = 0; i < splitQuestion.length; i++) 
    {
        
        if(splitQuestion.length != (i+1))
            splitInfo[4] += splitQuestion[i] + ",";
        else
            splitInfo[4] += splitQuestion[i]; 
    }
    splitEstimote[numberOfEstimote] = "";
    for (var i = 0; i < splitInfo.length; i++) 
    {
        
        if(splitInfo.length != (i+1))
            splitEstimote[numberOfEstimote] += splitInfo[i] + "-";
        else
            splitEstimote[numberOfEstimote] += splitInfo[i]; 
    }
    valueLocalStorage = "";
    for (var i = 0; i < splitEstimote.length; i++) 
    {
        
        if(splitEstimote.length != (i+1))
            valueLocalStorage += splitEstimote[i] + "|";
        else
            valueLocalStorage += splitEstimote[i]; 
    }
    //On renvoie la valeur dans le cookie.
    SetLocalStorage(nomLocalStorage, valueLocalStorage);
}
//On vérifie si l'utilisateur à répondu à toutes les questions de l'estimote. Si oui, on retourne true, sinon false 
function VerifieAnswer(estimote)
{
    var valueLocalStorage;
    var tableAnswer;
    var splitEstimote;
    var splitInfo;
    var splitQuestion;
    var splitQuestionAnswerNext;
    var nextPage = "attente";
    //valide que le cookie est bien initialisé.
    if(GetLocalStorage(nomLocalStorage) == "")
        Quitter();
    valueLocalStorage = GetLocalStorage(nomLocalStorage);
    splitEstimote = valueLocalStorage.split("|");
    //Permet de séparer le numéro de la question de la réponse à changer et de la mettre dans un tableau.
    splitInfo = splitEstimote[estimote].split("-");
    splitQuestion = splitInfo[4].split(",");
    for(var i = 0; i < splitQuestion.length ; i++)
    {
        splitQuestionAnswerNext = splitQuestion[i+1].split(":");
        if((splitQuestionAnswerNext[0] !==  undefined) && (splitQuestionAnswerNext[1] === undefined))
        {
            nextPage = splitQuestionAnswerNext[0];
            break;
        }
    }
    
    return nextPage;
}

//Fonction qui vérifie si la vidéo est déjà lue
function VerifieVideo(numberOfEstimote)
{
    var valueLocalStorage;
    var tableAnswer;
    var splitEstimote;
    var splitInfo;
    var splitVideo;
    var nextPage = "";
    //valide que le cookie est bien initialisé.
    if(GetLocalStorage(nomLocalStorage) == "")
        Quitter();
    valueLocalStorage = GetLocalStorage(nomLocalStorage);
    splitEstimote = valueLocalStorage.split("|");
    //Permet de séparer le numéro de la question de la réponse à changer et de la mettre dans un tableau.
    splitInfo = splitEstimote[numberOfEstimote].split("-");
    splitVideo = splitInfo[3].split(":");
    if(splitVideo[1] === undefined)
        nextPage = "video" + splitVideo[0];
    return nextPage;
}

//Fonction qui set les video aux bon estimotes 
function SetVideo(video)
{
    var valueLocalStorage;
    var tableAnswer;
    var numberOfEstimote;
    var splitEstimote;
    var splitInfo;
    var splitQuestion;
    var splitQuestionAnswer;
    var nextpage;
    //valide que le cookie est bien initialisé.
    if(GetLocalStorage(nomLocalStorage) == "")
        Quitter();
    numberOfEstimote = EstimoteToVideo(video);
    valueLocalStorage = GetLocalStorage(nomLocalStorage);
    splitEstimote = valueLocalStorage.split("|");
    //Permet de séparer le numéro de la question de la réponse à changer et de la mettre dans un tableau.
    splitInfo = splitEstimote[numberOfEstimote].split("-");
    splitInfo[3] = video + ":1";
    splitQuestion = splitInfo[4].split(",");
        for(var l = 0; l < splitQuestion.length ; l++)
        {
            splitQuestionAnswer = splitQuestion[l].split(":");
            if(splitQuestionAnswer[1] == undefined)
            {
                if(splitQuestionAnswer[0] != "")
                    nextpage = splitQuestionAnswer[0];
                else
                    nextpage = "attente";
                break;
            }
        }
    splitEstimote[numberOfEstimote] = "";
    for (var i = 0; i < splitInfo.length; i++) 
    {
        
        if(splitInfo.length != (i+1))
            splitEstimote[numberOfEstimote] += splitInfo[i] + "-";
        else
            splitEstimote[numberOfEstimote] += splitInfo[i]; 
    }
    valueLocalStorage = "";
    for (var i = 0; i < splitEstimote.length; i++) 
    {
        
        if(splitEstimote.length != (i+1))
            valueLocalStorage += splitEstimote[i] + "|";
        else
            valueLocalStorage += splitEstimote[i]; 
    }
    SetLocalStorage(nomLocalStorage, valueLocalStorage);
    ChangePage(nextpage);
}

//Permet d'indiquer a quel estimote appartient a quel question
function EstimoteToQuestion(question)
{
    var valueLocalStorage;
    var splitEstimote;
    var splitInfo;
    var splitQuestion;
    var splitQuestionAnswer;
    var valide = false;
    valueLocalStorage = GetLocalStorage(nomLocalStorage);
    //Permet de séparer chaque question et ça réponse et de les mettres dans un tableau. 
    splitEstimote = valueLocalStorage.split("|");
    for(var i = 0; i < splitEstimote.length ; i++)
    {
        splitInfo = splitEstimote[i].split("-");
        splitQuestion = splitInfo[4].split(",");
        for(var l = 0; l < splitQuestion.length ; l++)
        {
            splitQuestionAnswer = splitQuestion[l].split(":");
            if(splitQuestionAnswer[0] == question)
            {
                valide = true;
                break;
            }
        }
        if(valide)
            break;
    }
    if(valide)
        return  i;
    else
        return "";
}

//Permet d'indiquer quel estimote appartient a quelle video
function EstimoteToVideo(video)
{
    var valueLocalStorage;
    var splitEstimote;
    var splitInfo;
    var splitVideo;
    var valide = false;
    valueLocalStorage = GetLocalStorage(nomLocalStorage);
    //Permet de séparer chaque question et ça réponse et de les mettres dans un tableau. 
    splitEstimote = valueLocalStorage.split("|");
    for(var i = 0; i < splitEstimote.length ; i++)
    {
        splitInfo = splitEstimote[i].split("-");
        splitVideo = splitInfo[3].split(":");
        if(splitVideo[0] == video)
        {
            valide = true;
            break;
        }
        if(valide)
            break;
    }
    if(valide)
        return  i;
    else
        return "";
}
//Permet de changer de page tout en validant si il reste d'autres pages disponnible pour l'Estimote en question 
function NextPage(actualQuestion)
{
    var numberOfEstimote;
    var pageChange = "attente";
    var valueLocalStorage;
    var numberOfEstimote;
    var splitEstimote;
    var splitInfo;
    var splitQuestion;
    var splitQuestionAnswer;
    var splitQuestionAnswerNext;
    
    numberOfEstimote = EstimoteToQuestion(parseInt(actualQuestion));
    valueLocalStorage = GetLocalStorage(nomLocalStorage);
    splitEstimote = valueLocalStorage.split("|");
    splitInfo = splitEstimote[numberOfEstimote].split("-");
    splitQuestion = splitInfo[4].split(",");
    for(var i = 0; i < splitQuestion.length ; i++)
    {
        splitQuestionAnswer = splitQuestion[i].split(":");
        if(splitQuestionAnswer[0] == actualQuestion)
        {
            if(splitQuestion[i + 1] !==  undefined)
            {
                splitQuestionAnswerNext = splitQuestion[i + 1].split(":");
                if(splitQuestionAnswerNext[1] === undefined)
                    pageChange = splitQuestionAnswerNext[0] ;
            }
            break;
        }
        
    }
    ChangePage(pageChange);
}
function ValideFinishAllQuestion()
{
    var valueLocalStorage;
    var tableAnswer;
    var splitEstimote;
    var splitInfo;
    var splitQuestion;
    var splitQuestionAnswer;
    var splitResultQuestion;
    var valide = true;
    var compterResultat = 0;
    var compterQuestion = 0;
    //valide que le cookie est bien initialisé.
    if(GetLocalStorage(nomLocalStorage) == "")
        Quitter();
    valueLocalStorage = GetLocalStorage(nomLocalStorage);
    splitEstimote = valueLocalStorage.split("|");
    //Permet de séparer le numéro de la question de la réponse à changer et de la mettre dans un tableau.
    for(var i = 0; i < splitEstimote.length ; i++)
    {
        splitInfo = splitEstimote[i].split("-");
        splitQuestion = splitInfo[4].split(",");
        for(var l = 0; l < splitQuestion.length ; l++)
        {
            splitQuestionAnswer = splitQuestion[l].split(":");
            if(splitQuestionAnswer[0] != "")
            {
                if(splitQuestionAnswer[1] !== undefined)
                {
                    splitResultQuestion = splitQuestionAnswer[1].split("/"); 
                    compterResultat += parseInt(splitResultQuestion[0]);
                    compterQuestion += parseInt(splitResultQuestion[1]);
                }
                else
                {
                    valide = false;
                    break;
                }
            }
        }  
    }
    if(valide)
        return "resultat=" + compterResultat + "," + compterQuestion;
    else
        return "";
}
function ShowActualResultat()
{
    var valueLocalStorage;
    var tableAnswer;
    var splitEstimote;
    var splitInfo;
    var splitQuestion;
    var splitQuestionAnswer;
    var splitResultQuestion;
    var valide = true;
    var compterResultat = 0;
    var compterQuestion = 0;
    //valide que le cookie est bien initialisé.
    if(!confirm("Vous n'avez pas répondu à toutes les questions! \n En confirmant, votre résultat sera affiché et il ne sera plus possible de continuer le questionnaire. \n  Voulez-vous continuer?"))
        return "";
    if(GetLocalStorage(nomLocalStorage) == "")
        Quitter();
    valueLocalStorage = GetLocalStorage(nomLocalStorage);
    splitEstimote = valueLocalStorage.split("|");
    //Permet de séparer le numéro de la question de la réponse à changer et de la mettre dans un tableau.
    for(var i = 0; i < splitEstimote.length ; i++)
    {
        splitInfo = splitEstimote[i].split("-");
        splitQuestion = splitInfo[4].split(",");
        for(var l = 0; l < splitQuestion.length ; l++)
        {
            splitQuestionAnswer = splitQuestion[l].split(":");
            if(splitQuestionAnswer[0] != "")
            {
                if((splitQuestionAnswer[1] != "") && (splitQuestionAnswer[1] !== undefined))
                {
                    splitResultQuestion = splitQuestionAnswer[1].split("/"); 
                    compterResultat += parseInt(splitResultQuestion[0]);
                    compterQuestion += parseInt(splitResultQuestion[1]);
                }
            }
        }  
        
    }
    ChangePage("resultat=" + compterResultat + "," + compterQuestion);
}
//Cette fonction est appeler par la fonction displayBeaconList dans l'app.js, celle-ci est donc utiliser exclusivement par la page d'attente.html, 
//elle permet d'aller sur la première page pour chaque estimote. 
//Elle fait aussi valider si l'utilisateur est déjà été sur l'estimote en question en répondant soit au question et/ou en regardant la vidéo.
function ActionInRange(beacon)
{
    var meters = beacon.distance;
    var nextPage ="";
    var valueLocalStorage;
    var splitEstimote;
    var splitInfo;
    var distance = "";
    var numberOfEstimote = null;
    if(GetLocalStorage(nomLocalStorage) == "")
       Quitter();

    if(!meters)
        return '';
    if(inuse)
        return
    else
        inuse = true;
    valueLocalStorage = GetLocalStorage(nomLocalStorage);
    splitEstimote = valueLocalStorage.split("|");
    for(var i = 0; i < splitEstimote.length ; i++)
    {
        splitInfo = splitEstimote[i].split("-");
        if((beacon.major == splitInfo[0]) && (beacon.minor == splitInfo[1]))
        {
            distance = splitInfo[2];
            numberOfEstimote = i;
            break;
        }
    }
    if((meters <= distance) && (numberOfEstimote != null))
    {
        nextPage = VerifieVideo(numberOfEstimote);
        if(nextPage == null)
            nextPage = VerifieAnswer(numberOfEstimote); 
    }
    if(nextPage == "")
       nextPage = ValideFinishAllQuestion();
    
    if(nextPage != "")
        //On appelle la fonction qui permet de changer de page en lui envoyant le nom de la page voulu.
        ChangePage(nextPage);
    inuse = false;
}
//Cette fonction permet de changer de page active et d'aller sur la page qui lui est envoyé en paramètre.
function ChangePage(pagechange)
{
    var video;
    var finale;
    if(pagechange != "")
    { 
        if(isNaN(pagechange))
        {
            video = pagechange.split("video");
            finale = pagechange.split("resultat=");
            if (video[1] !== undefined)
            {
                window.location.replace("video.html?video=Video" + video[1]);
            }
            else if (finale[1] !== undefined)
            {
                window.location.replace("finale.html?Resultat=" + finale[1]);
            }
            else
            {
                window.location.replace(pagechange + ".html");
            }
        }
        else
        {
            window.location.replace("main.html?question=Question" + pagechange);
        }
    }
}
//Permet de réinitialiser les cookies à ca valeur de départ.
function Recommencer()
{
	if (confirm("Voulez-vous vraiment recommencer?")){
		SetLocalStorage(nomLocalStorage, "");
		ChangePage("index");
	}
}
//Permet de fermer l'application
/*function Quitter()
{
	SetLocalStorage(nomLocalStorage, "");
    ChangePage("index");
}*/
//à changer lorsqu'on buld avec cordova et mettre la ligne à la place de changerpage(... dans Quitter.
function ExitFromApp()
{
    navigator.app.exitApp();
}