/*****************************************************************************/
//	Auteur:				Michaël Bourgoin
//	Description:		XMl.js
//	Date de création:	Hivers 2015
//	But:				-Afficher et récupérer les données à partir d'un fichier xml 
//	
//	Version		Date 			Nom 					Description 
//	1.0			23 fev  2015	Michaël Bourgoin  		Ajout de la page , commentaire
//  2.0 		** mars 2015   	Michaël Bourgoin		Ajout de la gestion des images multiples ainsi que des réponses spécials
/*****************************************************************************/
		
//Instanciation de la variable xhr selon la platform utilisé.
function getXMLHttpRequest() 
{
    var xhr = null;
    if (window.XMLHttpRequest || window.ActiveXObject) {
        if (window.ActiveXObject) {
            try {
                xhr = new ActiveXObject("Msxml2.XMLHTTP");
            } catch(e) {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            }
        } else {
            xhr = new XMLHttpRequest(); 
        }
    } else {
        alert("Erreur, votre appareil ne peut pas utiliser l'application");
        return null;
    }
    return xhr;
}

//Récupération des données du fichier xml 
function request(callback, page) {			
    var params = window.location.search.substr(1).split('=');
    var xhr = getXMLHttpRequest();
    numQues = params[1];
    xhr.onreadystatechange = function() 
    {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
        {
            callback(xhr.responseXML);
        }
    };	
    xhr.open("GET", page + ".xml", true);
    xhr.send(null);
}

//Lecture du fichier xml et affichage des données désirées
function readDataQuestion(oData) 
{			
	var nodes = oData.getElementsByTagName(numQues);
    document.getElementById("NumeroQuestion").innerHTML = (/[0-9]+/.exec(numQues) == null ? "" : "Question " + /[0-9]+/.exec(numQues) );	
    
	for (z=0;z<nodes.length;z++)
	{		 		
		//Permet de créer le h2 (Titre#) qui contiendra plus tard la question
		var para = document.createElement ("h2");
		para.id = "Titre"+z;	
		var element = document.getElementById("Question");
		element.appendChild(para);	
				
		//Permet de créer les div qui contiendra toutes les images.
		var j=1;
		NbreImage=0;
		while (nodes[z].getElementsByTagName("Image"+j).length >0)
		{
			NbreImage++;
			j++;		
		}
		
		var para = document.createElement ("div");
		para.id = "video";
		var element = document.getElementById("Question");
		element.appendChild(para);	
		
		var para = document.createElement ("div");
		para.id = "LesImages"+z;
		var element = document.getElementById("Question");
		element.appendChild(para);			
				
		//Permet de créer un div qui contiendra toutes les choix de réponses.
		var para = document.createElement ("div")
		para.id = "Reponse"+z;	
		var element = document.getElementById("Question");
		element.appendChild(para);			
		for (var i=0, c=4; i<c; i++) 
		{
			if (i==0)
			{	
				//Permet de créer un div qui contient une question et de l'ajouter dans le h2 Titre# 
				var para = document.createElement ("div");
				var node = document.createTextNode(nodes[z].getElementsByTagName("Question")[0].childNodes[0].nodeValue);
				para.appendChild(node);
				var element = document.getElementById("Titre"+z);
				element.appendChild(para);
				
				
				//Permet d'ajouter les vidéos
				if (nodes[z].getElementsByTagName("Video").length >0)
				{  
                     var iOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false );
					 var para = document.createElement ("video");
                     if(iOS)
                    {   
                        para.className ="Video";
                        para.src = "video/" + nodes[z].getElementsByTagName("Video")[0].childNodes[0].nodeValue;
                        para.type ="video/mp4";	
                        para.autoplay=true;
                        para.controls=true;
                        var element = document.getElementById("video");
                        element.appendChild(para);
                    }
                    else
                    {
                        nomVideo = nodes[z].getElementsByTagName("Video")[0].childNodes[0].nodeValue;
						para.className ="Video";
                        para.id ="splashvideo";
                        para.type ="video/mp4";	
                        para.autoplay=true;
                        para.controls=true;
                        var element = document.getElementById("video");
                        element.appendChild(para);
						
						t = setTimeout(function(){ onDeviceReady() }, 150);
                    }
				}
			
				
				
				
				//Permet de d'ajouter les images 
				for (var u=1;u<=NbreImage;u++)
				{
					var para = document.createElement ("img");
					para.src  ="img/" +nodes[z].getElementsByTagName("Image"+u)[0].childNodes[0].nodeValue;		
					para.className = nodes[z].getElementsByTagName("TypeImage")[0].childNodes[0].nodeValue;
					var element = document.getElementById("LesImages"+z);
					element.appendChild(para);
				}	
			}
			else
			{
				//Permet de créer un div qui contient un choix de réponse et de l'ajouter dans le Lesquestion#
				TypeQuestion = nodes[z].getElementsByTagName("TypeQuestion")[0].childNodes[0].nodeValue;
				switch(TypeQuestion) 
				{
					case "Normal":
						var para = document.createElement ("div");
						para.id ="Q" + z + "R" + i;
						para.className = "question";
						para.onclick = (function() 
						{
							ChangerBouton(this.id);
						});
						var node = document.createTextNode(nodes[z].getElementsByTagName("Reponse"+i)[0].childNodes[0].nodeValue);
						para.appendChild(node);
						var element = document.getElementById("Reponse"+z);
						element.className = "Normal";
						element.appendChild(para);
						break;
						
					case "TextBox":
						var para = document.createElement ("INPUT");
						para.type = "text";
						para.name ="reponse";
						var element = document.getElementById("Reponse"+z);
						element.className = "TextBox";
						element.appendChild(para);
						i=4;
						break;
						
					case "NumBox":
						var para = document.createElement ("INPUT");
						para.type = "number";
						para.name ="reponse";
						var element = document.getElementById("Reponse"+z);
						element.className = "TextBox";
						element.appendChild(para);
						i=4;
						break;
						
					case "MultiTexteBox":
						var mot = nodes[z].getElementsByTagName("Reponse"+i)[0].childNodes[0].nodeValue;
						var value = nodes[z].getElementsByTagName("Value")[0].childNodes[0].nodeValue;
						for (b=0 ; b< mot.length ; b++)
						{
							if (value.indexOf(b+1)== -1)
							{
								var para = document.createElement ("INPUT");
								para.type = "text";
								para.name ="reponse"; 
								para.maxLength = "1";
								para.id = b;
								para.onkeyup = (function() 
								{
									ChangeTab(this.id);
								});
								
								var element = document.getElementById("Reponse"+z);
								element.appendChild(para);
							}
							else
							{
								var para = document.createElement ("INPUT");
								para.type = "text";
								para.name ="reponse"; 
								para.maxLength = "1";
								para.id = b;
								para.onkeyup = (function() 
								{
									ChangeTab(this.id);
								});
								para.value = mot.charAt(b);
								para.disabled = true ;
								var element = document.getElementById("Reponse"+z);
								element.className = "LesInputs";
								element.appendChild(para);
							}
							i=4;
						}
						
						break;
					case "Image" : 
						var para = document.createElement ("div");
						para.id ="Q" + z + "R" + i;
						para.className = "question";
						para.onclick = (function() 
						{
							ChangerBouton(this.id);
						});
						para.innerHTML = "<img id='Image' src='img/"+nodes[z].getElementsByTagName("Reponse"+i)[0].childNodes[0].nodeValue+"'/>" ;
						
						
						var element = document.getElementById("Reponse"+z);
						element.className = "Normal";
						element.appendChild(para);
						
						
						break;
					default:
						alert("Une erreur est survenue");
						
					
				} 				
				
				
			}		
		}				
	}
}
	function timedCount() 
		{
		document.addEventListener("deviceready", onDeviceReady, false);
		clearTimeout(t);
		}
				// device APIs are available
	function onDeviceReady() 
	{
	window.plugins.html5Video.initialize({
	"splashvideo" : nomVideo
		})
	t = setTimeout(function(){ window.plugins.html5Video.play("splashvideo"); }, 250);
	}

// Fait appelle a la page Answer et vérifie si les résultats des questions sont correct 
function readDataAnswer(oData) 
{
    var nodes = oData.getElementsByTagName(numQues);
    var answer = [];
    var numberGoodAnswer = 0;
    var fields = reponseFinale.split("|");
    var tempo;
    for(var i = 0; i < fields.length; i++) 
    {
        if (fields[i] == nodes[i].getElementsByTagName("Reponse")[0].childNodes[0].nodeValue)
        {
            answer[i] = 1;
            numberGoodAnswer ++;
        }
        else
        {
            answer[i] = 0;
        }
    }
    tempo = numQues.split("Question");
    AddAnswer(tempo[1], numberGoodAnswer, i);
    window.location.replace("answer.html" +"?question="+numQues +"&answer="+answer);
}
function readDataReponse(oData) 
{
    var tempo = numQues.split("&");
    numQues = tempo[0];
    var nodes = oData.getElementsByTagName(numQues);
    var ans = answer;
    var nbreAnswer = answer.split(',');
    for (i=0; i<(nbreAnswer.length);i++ )
    {
        var para = document.createElement ("h2");

        para.id = "Titre"+i;
        if (nbreAnswer[i] ==1)
        {
            para.innerHTML = "Vous avez bien répondu à la question !" ;
        }
        else
        {
            para.innerHTML = "Vous n'avez pas bien répondu à la question !";
            para.className = "red";
        }
        var element = document.getElementById("Reponses");
        element.appendChild(para);	
        var para = document.createElement ("p");
        para.id = "Reponse"+i;
        para.innerHTML = nodes[i].getElementsByTagName("Detail")[0].childNodes[0].nodeValue;
        var element = document.getElementById("Reponses");
        element.appendChild(para);	
    }

}
function readDataEstimote(xmlData) 
{
    var nodes = xmlData.getElementsByTagName("LesEstimotes");
    var nombreQuestion = 0;
    var estimotes = "";
    var lesQuestions = "";
    var questions = "";
    var nomCookie= "table";
    var valueTable = "";
    if((GetLocalStorage(nomCookie) == "") || (GetLocalStorage(nomCookie) == undefined) || (GetLocalStorage(nomCookie) == null))
    {
        for (var i = 0; i < nodes[0].getElementsByTagName("estimote").length; i++)
        {
            estimotes = nodes[0].getElementsByTagName("estimote")[i];
            valueTable += estimotes.getElementsByTagName("major")[0].childNodes[0].nodeValue;
            valueTable += "-" + estimotes.getElementsByTagName("minor")[0].childNodes[0].nodeValue;
            valueTable += "-" + estimotes.getElementsByTagName("distance")[0].childNodes[0].nodeValue;
            valueTable += "-" + estimotes.getElementsByTagName("video")[0].childNodes[0].nodeValue;
            valueTable += "-";
            lesQuestions = estimotes.getElementsByTagName("LesQuestions");
            for (var l = 0; l < lesQuestions[0].getElementsByTagName("question").length; l++)
            {
                questions = lesQuestions[0].getElementsByTagName("question")[l];
                if ((l + 1) != lesQuestions[0].getElementsByTagName("question").length)
                    valueTable += questions.childNodes[0].nodeValue + ",";
                else
                {
                    valueTable += questions.childNodes[0].nodeValue;
                }
            }
            if((i + 1) != nodes[0].getElementsByTagName("estimote").length)
           
                valueTable += "|";
        }
        SetLocalStorage(nomCookie, valueTable);
    }
}
